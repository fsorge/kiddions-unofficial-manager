#ifndef UTILS_H
#define UTILS_H

#include <fstream>

#include <QString>
#include <QDir>
#include <QTextStream>

#include "common.h"

class Utils
{
private:
    Utils();

    static const QStringList kiddionsMustHaveFiles;

public:

    static bool isKiddionsDir(QString);
    static bool fileExists(QString);
    static json fileToJson(QFile, QIODevice::OpenModeFlag);
    static bool jsonToFile(std::string, json);
};

#endif // UTILS_H
