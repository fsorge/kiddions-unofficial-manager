#include "utils.h"

const QStringList Utils::kiddionsMustHaveFiles = {"modest-menu.exe"};

Utils::Utils() { }

bool Utils::isKiddionsDir(QString dir) {
    const QStringList files = QDir(dir).entryList(Utils::kiddionsMustHaveFiles, QDir::Files);
    return files.length() > 0;
}

bool Utils::fileExists(QString path) {
    return QFile::exists(path);
}

json Utils::fileToJson(QFile file, QIODevice::OpenModeFlag type) {
    file.open(type);
    QByteArray rawData = file.readAll();
    return json::parse(rawData.toStdString());
}

bool Utils::jsonToFile(std::string path, json json) {
    std::ofstream myfile;
    myfile.open(path);
    myfile << json.dump(2);
    myfile.close();

    return true;
}
