#ifndef KIDDIONSVEHICLES_H
#define KIDDIONSVEHICLES_H

#include <QHash>

#include "../common.h"

namespace Kiddions {
    struct Vehicles {
        struct Vehicle {
            struct Flags {
                bool BulletproofTyres;
                bool CustomPrimaryColor;
                bool CustomSecondaryColor;
                bool ExtraFlag1;
                bool ExtraFlag2;
                bool ExtraFlag3;
                bool ExtraFlag4;
                bool ExtraFlag5;
                bool ExtraFlag6;
                bool ExtraFlag7;
                bool ExtraFlag8;
                bool ExtraFlag9;
                bool ExtraFlag10;
                bool ExtraFlag11;
                bool ExtraFlag12;
                bool NeonBack;
                bool NeonFront;
                bool NeonLeft;
                bool NeonRight;
            };

            struct RGB {
                int r;
                int g;
                int b;
            };

            int DashboardColorIndex;
            float EnvEffScale;
            Flags flags;
            int InteriorColorIndex;
            long ModelHash;
            std::string Name;
            RGB NeonColor;
            int NumberPlateIndex;
            std::string NumberPlateText;
            int PearlesentColorIndex;
            RGB PrimaryColor;
            int PrimaryColorIndex;
            RGB SecondaryColor;
            int SecondaryColorIndex;
            RGB TyreSmokeColor;
            std::vector<int> VehicleMods;
            int WheelColorIndex;
            int WheelType;
            std::vector<int> WheelVariations;
            int WindowTint;
        };

        std::vector<struct Vehicle> CustomVehicles;
    };

    inline bool operator==(const Vehicles::Vehicle& lhs, const Vehicles::Vehicle& rhs){
        return lhs.Name == rhs.Name;
    }

    inline void from_json(const json& j, struct Vehicles::Vehicle::RGB& val) {
        j.at("r").get_to(val.r);
        j.at("g").get_to(val.g);
        j.at("b").get_to(val.b);
    }

    inline void to_json(json& j, const Vehicles::Vehicle::RGB& val) {
        j = json{
        {"r", val.r},
        {"g", val.g},
        {"b", val.b},
        };
    }

    inline void from_json(const json& j, struct Vehicles::Vehicle::Flags& val) {
        j.at("BulletproofTyres").get_to(val.BulletproofTyres);
        j.at("CustomPrimaryColor").get_to(val.CustomPrimaryColor);
        j.at("CustomSecondaryColor").get_to(val.CustomSecondaryColor);
        j.at("ExtraFlag1").get_to(val.ExtraFlag1);
        j.at("ExtraFlag2").get_to(val.ExtraFlag2);
        j.at("ExtraFlag3").get_to(val.ExtraFlag3);
        j.at("ExtraFlag4").get_to(val.ExtraFlag4);
        j.at("ExtraFlag5").get_to(val.ExtraFlag5);
        j.at("ExtraFlag6").get_to(val.ExtraFlag6);
        j.at("ExtraFlag7").get_to(val.ExtraFlag7);
        j.at("ExtraFlag8").get_to(val.ExtraFlag8);
        j.at("ExtraFlag9").get_to(val.ExtraFlag9);
        j.at("ExtraFlag10").get_to(val.ExtraFlag10);
        j.at("ExtraFlag11").get_to(val.ExtraFlag11);
        j.at("ExtraFlag12").get_to(val.ExtraFlag12);
        j.at("NeonBack").get_to(val.NeonBack);
        j.at("NeonFront").get_to(val.NeonFront);
        j.at("NeonLeft").get_to(val.NeonLeft);
        j.at("NeonRight").get_to(val.NeonRight);
    }

    inline void to_json(json& j, const Vehicles::Vehicle::Flags& val) {
        j = json{
        {"BulletproofTyres", val.BulletproofTyres},
        {"CustomPrimaryColor", val.CustomPrimaryColor},
        {"CustomSecondaryColor", val.CustomSecondaryColor},
        {"ExtraFlag1", val.ExtraFlag1},
        {"ExtraFlag2", val.ExtraFlag2},
        {"ExtraFlag3", val.ExtraFlag3},
        {"ExtraFlag4", val.ExtraFlag4},
        {"ExtraFlag5", val.ExtraFlag5},
        {"ExtraFlag6", val.ExtraFlag6},
        {"ExtraFlag7", val.ExtraFlag7},
        {"ExtraFlag8", val.ExtraFlag8},
        {"ExtraFlag9", val.ExtraFlag9},
        {"ExtraFlag10", val.ExtraFlag10},
        {"ExtraFlag11", val.ExtraFlag11},
        {"ExtraFlag12", val.ExtraFlag12},
        {"NeonBack", val.NeonBack},
        {"NeonFront", val.NeonFront},
        {"NeonLeft", val.NeonLeft},
        {"NeonRight", val.NeonRight}
        };
    }

    inline void from_json(const json& j, struct Vehicles::Vehicle& val) {
        j.at("DashboardColorIndex").get_to(val.DashboardColorIndex);
        j.at("EnvEffScale").get_to(val.EnvEffScale);
        j.at("Flags").get_to(val.flags);
        j.at("InteriorColorIndex").get_to(val.InteriorColorIndex);
        j.at("ModelHash").get_to(val.ModelHash);
        j.at("Name").get_to(val.Name);
        j.at("NeonColor").get_to(val.NeonColor);
        j.at("NumberPlateIndex").get_to(val.NumberPlateIndex);
        j.at("NumberPlateText").get_to(val.NumberPlateText);
        j.at("PearlesentColorIndex").get_to(val.PearlesentColorIndex);
        j.at("PrimaryColor").get_to(val.PrimaryColor);
        j.at("PrimaryColorIndex").get_to(val.PrimaryColorIndex);
        j.at("SecondaryColor").get_to(val.SecondaryColor);
        j.at("SecondaryColorIndex").get_to(val.SecondaryColorIndex);
        j.at("TyreSmokeColor").get_to(val.TyreSmokeColor);
        j.at("VehicleMods").get_to(val.VehicleMods);
        j.at("WheelColorIndex").get_to(val.WheelColorIndex);
        j.at("WheelType").get_to(val.WheelType);
        j.at("WheelVariations").get_to(val.WheelVariations);
        j.at("WindowTint").get_to(val.WindowTint);
    }

    inline void to_json(json& j, const Vehicles::Vehicle& val) {
        j = json{
        {"DashboardColorIndex", val.DashboardColorIndex},
        {"EnvEffScale", val.EnvEffScale},
        {"Flags", val.flags},
        {"InteriorColorIndex", val.InteriorColorIndex},
        {"ModelHash", val.ModelHash},
        {"Name", val.Name},
        {"NeonColor", val.NeonColor},
        {"NumberPlateIndex", val.NumberPlateIndex},
        {"NumberPlateText", val.NumberPlateText},
        {"PearlesentColorIndex", val.PearlesentColorIndex},
        {"PrimaryColor", val.PrimaryColor},
        {"PrimaryColorIndex", val.PrimaryColorIndex},
        {"SecondaryColor", val.SecondaryColor},
        {"SecondaryColorIndex", val.SecondaryColorIndex},
        {"TyreSmokeColor", val.TyreSmokeColor},
        {"VehicleMods", val.VehicleMods},
        {"WheelColorIndex", val.WheelColorIndex},
        {"WheelType", val.WheelType},
        {"WheelVariations", val.WheelVariations},
        {"WindowTint", val.WindowTint}
        };
    }

    inline void from_json(const json& j, struct Vehicles& val) {
        j.at("CustomVehicles").get_to(val.CustomVehicles);
    }

    inline void to_json(json& j, const Vehicles& val) {
        j = json{{"CustomVehicles", val.CustomVehicles}};
    }
}

#endif // KIDDIONSVEHICLES_H
