#ifndef KIDDIONSCONFIG_H
#define KIDDIONSCONFIG_H

#include "../common.h"

namespace Kiddions {
    struct Config {
        struct Menu {
            struct KeyBindings {
                int BackKey;
                int DownKey;
                int LeftKey;
                int MenuToggle;
                int RightKey;
                int SelectKey;
                int UpKey;
            };

            struct KeyBindings Keybindings;
        };

        struct Hotkey {
            std::string Action;
            int VirtualKey;
        };

        struct Menu menu;
        std::vector<struct Config::Hotkey> Hotkeys;

        int* get_by_key_binding_name(std::string key) {
            if (key == "BackKey") {
                return &menu.Keybindings.BackKey;
            } else if (key == "DownKey") {
                return &menu.Keybindings.DownKey;
            } else if (key == "LeftKey") {
                return &menu.Keybindings.LeftKey;
            } else if (key == "MenuToggle") {
                return &menu.Keybindings.MenuToggle;
            } else if (key == "RightKey") {
                return &menu.Keybindings.RightKey;
            } else if (key == "SelectKey") {
                return &menu.Keybindings.SelectKey;
            } else if (key == "UpKey") {
                return &menu.Keybindings.UpKey;
            }

            return nullptr;
        }
    };

    inline void from_json(const json& j, struct Config::Menu::KeyBindings& val) {
        j.at("BackKey").get_to(val.BackKey);
        j.at("DownKey").get_to(val.DownKey);
        j.at("LeftKey").get_to(val.LeftKey);
        j.at("MenuToggle").get_to(val.MenuToggle);
        j.at("RightKey").get_to(val.RightKey);
        j.at("SelectKey").get_to(val.SelectKey);
        j.at("UpKey").get_to(val.UpKey);
    }

    inline void to_json(json& j, const Config::Menu::KeyBindings& val) {
        j = json{
        {"BackKey", val.BackKey},
        {"DownKey", val.DownKey},
        {"LeftKey", val.LeftKey},
        {"MenuToggle", val.MenuToggle},
        {"RightKey", val.RightKey},
        {"SelectKey", val.SelectKey},
        {"UpKey", val.UpKey}
        };
    }

    inline void from_json(const json& j, struct Config::Menu& val) {
        j.at("KeyBindings").get_to(val.Keybindings);
    }

    inline void to_json(json& j, const Config::Menu& val) {
        j = json{{"KeyBindings", val.Keybindings}};
    }

    inline void from_json(const json& j, struct Config::Hotkey& val) {
        j.at("Action").get_to(val.Action);
        j.at("VirtualKey").get_to(val.VirtualKey);
    }

    inline void to_json(json& j, const Config::Hotkey& val) {
        j = json{{"Action", val.Action}, {"VirtualKey", val.VirtualKey}};
    }

    inline void from_json(const json& j, struct Config& val) {
        j.at("Hotkeys").get_to(val.Hotkeys);
        j.at("Menu").get_to(val.menu);
    }

    inline void to_json(json& j, const Config& val) {
        j = json{{"Hotkeys", val.Hotkeys}, {"Menu", val.menu}};
    }
}

#endif // KIDDIONSCONFIG_H
