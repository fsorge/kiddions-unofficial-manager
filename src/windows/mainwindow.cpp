#include "mainwindow.h"
#include "ui_mainwindow.h"

QMap<std::string, QLineEdit*> txtBoxMappings;

QString kiddionsDir;
Kiddions::Config kiddionsConfig;
Kiddions::Vehicles kiddionsVehicles;
QListWidgetItem *selectedVehicle = nullptr;

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    this->updateLayout(false);

    txtBoxMappings.insert("LeftKey", ui->leftTxtBox);
    txtBoxMappings.insert("RightKey", ui->rightTxtBox);
    txtBoxMappings.insert("UpKey", ui->upTxtBox);
    txtBoxMappings.insert("DownKey", ui->downTxtBox);
    txtBoxMappings.insert("MenuToggle", ui->toggleTxtBox);
    txtBoxMappings.insert("SelectKey", ui->selectTxtBox);
    txtBoxMappings.insert("BackKey", ui->backTxtBox);

    for (auto it = txtBoxMappings.cbegin(); it != txtBoxMappings.cend(); it++) {
        (*it)->installEventFilter(this);
    }
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_action_Open_Folder_triggered()
{
    QString dir = QFileDialog::getExistingDirectory(this, tr("Open Kiddion's Directory"),
                                                    QStandardPaths::writableLocation(QStandardPaths::DesktopLocation),
                                                    QFileDialog::ShowDirsOnly | QFileDialog::ReadOnly);

    if (dir.length() > 0) {
        if (Utils::isKiddionsDir(dir)) {
            this->updateLayout(true);
            kiddionsDir = dir;
            this->loadFiles();
        } else {
            QMessageBox(QMessageBox::Icon::Critical, "Directory not valid", "This does not seem to be a valid Kiddion's Modest Menu directory. Try again.")
                    .exec();
        }
    }
}

void MainWindow::updateLayout(bool folderSelected) const {
    ui->mainTab->setVisible(folderSelected);
    ui->welcomeWidget->setVisible(!folderSelected);
    ui->action_Open_Folder->setEnabled(!folderSelected);
    ui->action_Save->setEnabled(folderSelected);
}

void MainWindow::loadFiles() const {
    QString configPath = QString{} + kiddionsDir + "/config.json";
//    QString teleportsPath = QString{} + kiddionsDir + "/teleports.json";
    QString vehiclesPath = QString{} + kiddionsDir + "/vehicles.json";

    if (Utils::fileExists(configPath)) {
        json config = Utils::fileToJson(
                    QFile(configPath), QIODevice::ReadWrite
                    );

        this->updateUIConfig(config.get<struct Kiddions::Config>());
    }

    if (Utils::fileExists(vehiclesPath)) {
        json config = Utils::fileToJson(
                    QFile(vehiclesPath), QIODevice::ReadWrite
                    );

        this->updateUIVehicles(config.get<struct Kiddions::Vehicles>());
    }
}

void MainWindow::updateUIConfig(struct Kiddions::Config config) const {
    kiddionsConfig = config;

    for (QMap<std::string, QLineEdit*>::iterator i = txtBoxMappings.begin(); i != txtBoxMappings.end(); ++i) {
        i.value()->setText(QString::number(*kiddionsConfig.get_by_key_binding_name(i.key())));
    }
}

void MainWindow::updateUIVehicles(struct Kiddions::Vehicles vehicles) const {
    kiddionsVehicles = vehicles;

    ui->vehiclesListWidget->clear();

    for (auto it = vehicles.CustomVehicles.cbegin(); it != vehicles.CustomVehicles.cend(); it++) {
        ui->vehiclesListWidget->addItem(QString::fromStdString(it->Name));
    }
}

void MainWindow::updateKeyBindings(QLineEdit* txtBox, QKeyEvent* key) const {
    std::string selection = txtBoxMappings.keys().at(txtBoxMappings.values().indexOf(txtBox));

    int* k = kiddionsConfig.get_by_key_binding_name(selection);
    *k = key->nativeVirtualKey();
    txtBox->setText(QKeySequence(key->key()).toString());

    qDebug() << *kiddionsConfig.get_by_key_binding_name(selection);
}

bool MainWindow::eventFilter(QObject *obj, QEvent *event)
{
    if (event->type() == QEvent::KeyPress) {
        QLineEdit* txtBox = dynamic_cast<QLineEdit*>(obj);
        if (txtBox != nullptr && txtBoxMappings.values().contains(txtBox)) {
            updateKeyBindings(
                        txtBox,
                        static_cast<QKeyEvent*>(event)
                        );
            return true;
        }
        // pass the event on to the parent class
        return QMainWindow::eventFilter(obj, event);
    }

    return false;
}

void MainWindow::on_removeDuplicatesBtn_clicked()
{
    QVector<struct Kiddions::Vehicles::Vehicle> seen;

    for (unsigned int i = 0; i < kiddionsVehicles.CustomVehicles.size(); i++) {
        const auto v = kiddionsVehicles.CustomVehicles.at(i);

        bool alreadySeen = false;
        for (auto it = seen.cbegin(); it != seen.cend(); it++) {
            if (v == *it) {
                alreadySeen = true;
                break;
            }
        }

        if (!alreadySeen) {
            seen.push_back(v);
        }
    }

    const int diff = kiddionsVehicles.CustomVehicles.size() - seen.size();

    if (diff > 0) {
        QMessageBox::StandardButton reply = QMessageBox::question(
                    this,
                    "Confirmation",
                    "We found " + QString::number(diff) + " duplicates. Would you like to purge them?",
                    QMessageBox::Yes|QMessageBox::No
                    );
        if (reply == QMessageBox::Yes) {
            kiddionsVehicles.CustomVehicles.clear();
            for (auto it = seen.cbegin(); it != seen.cend(); it++) {
                kiddionsVehicles.CustomVehicles.push_back(*it);
            }
            this->updateUIVehicles(kiddionsVehicles);
        }
    } else {
        QMessageBox(QMessageBox::Icon::Information, "Information", "No duplicates were found.")
                .exec();
    }

}

void MainWindow::on_vehiclesListWidget_currentItemChanged(QListWidgetItem *current, QListWidgetItem *) const
{
    selectedVehicle = current;

    this->updateVehiclesButtonsUI(current != nullptr);
}

void MainWindow::updateVehiclesButtonsUI(bool enable) const {
    ui->mvVecUpBtn->setEnabled(enable);
    ui->editVecBtn->setEnabled(enable);
    ui->rmVecBtn->setEnabled(enable);
    ui->mvVecDownBtn->setEnabled(enable);
}

void MainWindow::on_mvVecUpBtn_clicked() const
{
    if (selectedVehicle != nullptr) {
        unsigned int i = ui->vehiclesListWidget->selectionModel()->selectedIndexes().at(0).row();

        if (i > 0) {
            auto itPos = kiddionsVehicles.CustomVehicles.begin() + (i-1);
            kiddionsVehicles.CustomVehicles.insert(itPos, kiddionsVehicles.CustomVehicles.at(i));

            kiddionsVehicles.CustomVehicles.erase(kiddionsVehicles.CustomVehicles.begin() + (i+1));

            this->updateUIVehicles(kiddionsVehicles);

            ui->vehiclesListWidget->item(i-1)->setSelected(true);
        }
    }
}

void MainWindow::on_rmVecBtn_clicked() const
{
    if (selectedVehicle != nullptr) {
        int i = ui->vehiclesListWidget->selectionModel()->selectedIndexes().at(0).row();

        kiddionsVehicles.CustomVehicles.erase(kiddionsVehicles.CustomVehicles.begin() + i);

        this->updateUIVehicles(kiddionsVehicles);
    }
}

void MainWindow::on_mvVecDownBtn_clicked() const
{
    if (selectedVehicle != nullptr) {
        unsigned int i = ui->vehiclesListWidget->selectionModel()->selectedIndexes().at(0).row();

        if (i < kiddionsVehicles.CustomVehicles.size() - 1) {
            auto itPos = kiddionsVehicles.CustomVehicles.begin() + (i+2);
            kiddionsVehicles.CustomVehicles.insert(itPos, kiddionsVehicles.CustomVehicles.at(i));

            kiddionsVehicles.CustomVehicles.erase(kiddionsVehicles.CustomVehicles.begin() + i);

            this->updateUIVehicles(kiddionsVehicles);

            ui->vehiclesListWidget->item(i+1)->setSelected(true);
        }
    }
}

void MainWindow::on_editVecBtn_clicked()
{
    if (selectedVehicle != nullptr) {
        unsigned int i = ui->vehiclesListWidget->selectionModel()->selectedIndexes().at(0).row();

        bool ok;
        QString text = QInputDialog::getText(this, tr("Change vehicle name"),
                                           tr("Vehicle name:"), QLineEdit::Normal,
                                           QString::fromStdString(kiddionsVehicles.CustomVehicles.at(i).Name), &ok);
        if (ok && !text.isEmpty()) {
            kiddionsVehicles.CustomVehicles.at(i).Name = text.toStdString();
            this->updateUIVehicles(kiddionsVehicles);
        }
    }
}

void MainWindow::on_formatVecBtn_clicked()
{
    QMessageBox::StandardButton reply = QMessageBox::question(
                this,
                "Confirmation",
                "Formatting all vehicles will remove all Unknown [%NAME%] and will name them %NAME%. Do you want to proceed?",
                QMessageBox::Yes|QMessageBox::No
                );
    if (reply == QMessageBox::Yes) {
        std::vector<struct Kiddions::Vehicles::Vehicle> newVehicles;

        for (auto it = kiddionsVehicles.CustomVehicles.begin(); it != kiddionsVehicles.CustomVehicles.end(); it++) {
            QString name = QString::fromStdString(it->Name);

            if (name.startsWith("Unknown [") && name.endsWith("]")) {
                name = name.mid(9);
                name = name.mid(0, name.size() - 1);
            }

            it->Name = name.toStdString();

            newVehicles.push_back(*it);
        }
        kiddionsVehicles.CustomVehicles = newVehicles;
        this->updateUIVehicles(kiddionsVehicles);
    }
}

void MainWindow::on_action_Save_triggered() const
{
    QMap<QString, json> allPaths = {
        {QString{} + kiddionsDir + "/config.json", (json)kiddionsConfig},
        /*QString{} + kiddionsDir + "/teleports.json", */
        {QString{} + kiddionsDir + "/vehicles.json", (json)kiddionsVehicles}
    };
    QVector<QString> errorsPaths;

    for(auto it = allPaths.constKeyValueBegin(); it != allPaths.constKeyValueEnd(); it++) {
        auto original = Utils::fileToJson(QFile(it->first), QIODevice::ReadOnly);
        original.merge_patch(it->second);

        bool resp = Utils::jsonToFile(it->first.toStdString(), original);

        if (!resp) {
            errorsPaths.push_back(it->first);
        }
    }

    if (errorsPaths.size() == 0) {
        QMessageBox(QMessageBox::Icon::Information, "Saved", "Configurations saved.")
                .exec();
    } else {
        QMessageBox(QMessageBox::Icon::Critical, "Error", "Could not save configurations. Try again.\nWe could not save:\n" + errorsPaths.join("\n"))
                .exec();
    }
}
