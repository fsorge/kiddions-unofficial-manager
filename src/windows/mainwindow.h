#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QFileDialog>
#include <QStandardPaths>
#include <QMessageBox>
#include <QInputDialog>
#include <QLineEdit>
#include <QKeyEvent>
#include <QMap>
#include <QSet>
#include <QListWidgetItem>
#include <QStringListModel>

#include <fstream>

#include "../common.h"
#include "../utils.h"
#include "../models/KiddionsConfig.h"
#include "../models/KiddionsVehicles.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_action_Open_Folder_triggered();

    void on_removeDuplicatesBtn_clicked();

    void on_vehiclesListWidget_currentItemChanged(QListWidgetItem *current, QListWidgetItem *previous) const;

    void on_mvVecUpBtn_clicked() const;

    void on_rmVecBtn_clicked() const;

    void on_mvVecDownBtn_clicked() const;

    void on_editVecBtn_clicked();

    void on_formatVecBtn_clicked();

    void on_action_Save_triggered() const;

private:
    Ui::MainWindow *ui;

    bool eventFilter(QObject *, QEvent *);

    void updateLayout(bool) const;
    void loadFiles() const;
    void updateUIConfig(struct Kiddions::Config) const;
    void updateUIVehicles(struct Kiddions::Vehicles) const;
    void updateVehiclesButtonsUI(bool) const;
    void updateKeyBindings(QLineEdit*, QKeyEvent*) const;
};
#endif // MAINWINDOW_H
