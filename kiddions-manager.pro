QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++20
CONFIG += conan_basic_setup
include(conanbuildinfo.pri)

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    src/main.cpp \
    src/utils.cpp \
    src/windows/mainwindow.cpp

HEADERS += \
    src/common.h \
    src/models/KiddionsConfig.h \
    src/models/KiddionsVehicles.h \
    src/utils.h \
    src/windows/mainwindow.h

FORMS += \
    src/windows/mainwindow.ui

TRANSLATIONS += \
    i18n/kiddions-manager_it_IT.ts

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    icons.qrc

RC_ICONS = icons/app/icon.ico
