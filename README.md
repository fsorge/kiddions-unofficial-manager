# Kiddion's Unofficial Manager

![screen](assets/images/screen.png)

A simple tool for managing Kiddion's Modest Menu key bindings, vehicles and teleport locations.



## Download

You can grab the latest version of Kiddion's Unofficial Manager on the [Releases page](https://gitlab.com/fsorge/kiddions-unofficial-manager/-/releases).



## Develop

Kiddion's Unofficial Manager is made with C++ and Qt.

If you have experience with the technologies used in the project and would like to help developing the tool, clone this repository and then execute those two simple commands:

1. Install dependencies via [Conan Package Manager](https://conan.io/)

```bash
conan install .
```

2. Configure Conan to work with [QMake](https://doc.qt.io/qt-5/qmake-manual.html)

```bash
conan install . -g qmake
```

3. Download and Install the open source versions of Qt 6.0.3 toolchain and Qt Creator from the [official website](https://www.qt.io/)

4. Open the project with Qt Creator and you should be able to build and launch it correctly



## Disclaimer

Kiddion's Unofficial Manager is not affiliated in any way with Kiddion and Kiddion's Modest Menu.

Kiddion's Unofficial Manager and its developer does not promote cheating in online games in any way.

Use at your own risk.